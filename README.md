# Property_Filter

#Python
#Data Process
#Filter
#Output as Csv



## Usage

Input: Property txns data in taiwan 

Filter A: 
【主要用途】為【住家用】
【建物型態】為【住宅大樓】
【總樓層數】需【大於等於十三層】

Filter B:
計算【總件數】
計算【總車位數】(透過交易筆棟數)
計算【平均總價元】
計算【平均車位總價元】

Output: filter_a.csv, filter_b.csv

## Packages

Regular Expression: to capture the data
Pandas: to manipulate data


## Getting started

python version: 3.9.7

command:

```
python property_filter.py
```

data resource: https://plvr.land.moi.gov.tw/DownloadOpenData (台北市、新北市、桃園市、台中市、高雄市)


## Project status

Completed
