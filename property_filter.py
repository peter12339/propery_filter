#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 14:24:56 2022

@author: linjingkai
"""




import pandas as pd
import re


df_all = pd.DataFrame() # 建立DataFrame變數


for i in ['a','b','e','f','h']: #依序將資料集讀入dataframe變數中
    df_temp = pd.read_csv('/Users/linjingkai/Desktop/國泰考試/data/Property/'+i+"_lvr_land_a.csv",skiprows=[1])
    df_all = pd.concat([df_all,df_temp])

df_all = df_all.reset_index(drop=True) #重新設置index，以解決index重複之問題
    
    
def toNumber(floor): # 本函式將樓層數中文字轉換成整數型態，以供運算
    i=0
    result = 0
    number_dict={'一':1,'二':2,'三':3,'四':4,'五':5,'六':6,'七':7,'八':8,'九':9,'層':0}
    for i in range(len(floor)):
        if floor[i]=="十" and i==0:
            result += 10
        else:
            if floor[i]=="十":
                result=result*10
            else:
                result+=number_dict[floor[i]]
    return result


# Filter A
# 依條件萃取目標資料，分別為主要用途為住家用和建物型態為住宅大樓
# (後者使用Regular Expression方式萃取，只要開頭是"住宅大樓"皆納入DF之中，以策"住宅大樓"後方文字不全然相同，雖經過df_all["建物型態"].value_counts()語法確認過後，後方文字皆為"(11層含以上有電梯)")
temp =  df_all[  (df_all["主要用途"]=="住家用") & ( df_all["建物型態"].str.match(r"住宅大樓."))]
# 透過自訂義函數toNumber()處理後塞選總樓層數大於等於十三樓者
temp = temp[temp["總樓層數"].apply(toNumber)>=13]
temp = temp.reset_index(drop=True)
# 進行csv輸出
temp.to_csv("filter_a.csv",encoding="utf_8_sig" ,index=False)






# Filter B

def parking(input): # 本函數將"交易棟筆數"欄位中的資料進行處理以取得該交易車位數量，並轉換成數值型態，以便計算
    result = input.split("車位")
    return int(result[1])

df_summary = pd.DataFrame() # 建立目標DF

df_summary["總件數"] = [len(df_all)] # 透過計算總資料長度，取得df_all總交易件數

df_summary["總車位數"]=[df_all["交易筆棟數"].apply(parking).sum()] # 透過自訂義函數parking()取得所有交易中的車位總數（交易筆棟數中車位數量總和）

df_summary["平均總價元"] = [df_all["總價元"].mean()] # 將所有交易之總價元進行加總後，除以先前計算之總件數得之


# 進行"平均車位總價元"計算前，發現有些交易(帶車位)在"車位總價元"欄位中資料為零無法納入平均計算
temp = df_all[(df_all["交易筆棟數"].apply(parking)>0)] # 萃取所有帶車位之交易
temp["車位總價元"].describe() # 發現最小值到第一個quartile值皆為零


# 故將資料依據以下兩個條件進行萃取，1.交易中帶有車位 2.車位總價元大於零，以提高"平均車位總價元"真實性
df_temp = df_all[(df_all["交易筆棟數"].apply(parking)>0) & (df_all["車位總價元"]>0)] 
df_summary["平均車位總價元"]=[df_temp["車位總價元"].sum()/df_temp["交易筆棟數"].apply(parking).sum()]


#進行csv輸出
df_summary.to_csv("filter_b.csv", encoding="utf_8_sig", index=False)









